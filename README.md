# Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Notes before starting

As api key is private ,Please create a env file( `.env`, `env.development` or `.env.development.local` ) and add the api key named as `REACT_APP_API_KEY` key. Then app will be able to fetch data.
```
REACT_APP_API_KEY="api-key ...."
```


## Available Scripts

In the project directory, you can run:

### `npm install`

Installs the apps dependencies.

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm test`

Launches the test runner in the interactive watch mode.\

# Tell Me About It

This is a minimal app showing a list of people, with ability to fitler by two inputs, sort and showing in diffrent views(grid|list).



It works well on mobile and it's responsive. You can find tools in the side menu when using mobile

## Development

I used typescript and CRA for creating the project. I started with the main ts file `App.tsx` and added tailwind for the styles.
Folder `src/components` include the views we need in app.

I used hooks for handling states ,filterInputs and useEffects for fetching and monitoring data.

A folder name `util` contains sort and filter tools ts and test files.

I created `constants.ts` and `types.ts` for mutual variables.

`react-lazyload` is used for rendering avatar images.

There is an `error page` for the times you don't have an api key. You need to set the key and var in env.

I didn't use `react-router` as now we didn't have many pages.

## Stories I choosed

### `_design/accessibility`

- Responsive design, works on mobile and tablets

\
I choosed this so anyone who tries to visit it on a mobile device will have a proper experience

### `_design/accessibility`

- Sort by name and office
- Filter by name and office
- Enable switch between a grid and and a different view (such as list)

\
I choosed these for a better and easier searching and viewing profiles experience

### `_design/accessibility`

- Use Typescript (or similar, no any’s!)
- Unit tests for existing functionality

\
I choosed these because typescript helps you deliver better code in shorter time.
Also wrote test for filter and sort to make sure everything works fine.

## Other

- I noticed that we are fetching and showing around 300 profiles and we can have better performance using `pagination` or using libraries such as `react-window`, `react-virtualized` and ...
  
- I could use debounce for typing in filter inputs for faster result.
  
- I thought we can have a `env.development` and keys there so CI/CD customazation would be easier.

I didn't work on these issues (And more...) as that was not mentioned in stories and it was a little time consuming. I suggest considering them for further development.
