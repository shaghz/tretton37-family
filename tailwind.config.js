module.exports = {
  content: ["./src/**/*.{tsx,ts}"],
  theme: {
    extend: {
      colors: {
        trettonBlue: { 800: "#002B55" },
        trettonGreen: { 800: "#80C565" },
      },
    },
  },
  plugins: [],
};
