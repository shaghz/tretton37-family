export type Person = {
  name: string;
  email: string;
  orgUnit: string;
  office: string;
  highlighted: boolean;
  published: boolean;
  phoneNumber: string | null;
  manager: string | null;
  mainText: string | null;
  gitHub: string | null;
  twitter: string | null;
  stackOverflow: string | null;
  linkedIn: string | null;
  imagePortraitUrl: string | null;
  imageWallOfLeetUrl: string | null;
};

export interface FilterState {
  [key: string]: string;
}