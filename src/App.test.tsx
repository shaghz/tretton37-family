import React from 'react';
import { render, screen } from '@testing-library/react';
import App from './App';

test('renders Forbidden page', () => {
  render(<App />);
  const linkElement = screen.getByText(/Forbidden/i);
  expect(linkElement).toBeInTheDocument();
});
