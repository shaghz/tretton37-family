import { Person } from "../types";

export const sortByValue = (
  sortBy: "office" | "name",
  list: Person[]
): Person[] => {
  return list.sort((a: Person, b: Person) =>
    a[sortBy].localeCompare(b[sortBy])
  );
};
