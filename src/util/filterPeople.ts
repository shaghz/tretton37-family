import { FilterState, Person } from "../types";

export const filterPeople = (
  filterInput: FilterState,
  list: Person[]
) => {
  return list.filter((person: Person) => {
    return (
      person.name.toLowerCase().includes(filterInput.name.toLowerCase()) &&
      person.office.toLowerCase().includes(filterInput.office.toLowerCase())
    );
  });
};
