import { filterPeople } from "./filterPeople";

const list = [
  {
    name: "Deniz Yildirim",
    office: "Lund",
    email: "agron.kabashi@tretton37.com",
    phoneNumber: "+46725133736",
    manager: "lotta.wennolf@tretton37.com",
    orgUnit: "/Employees",
    mainText:
      "\u003cp\u003eI have been working in the software industry for over 10 years now, and even though the road has been bumpy, I don’t regret a single second of it! My experiences have been quite varied, from creating games to full blown web-based intranet platforms.\u003c/p\u003e\u003cp\u003eBecause of this variation and these experiences I’ve found that I’ve acquired a thirst for front-end development and anything web related, which can range from mobile apps to web-based SaaS solutions. The fact that you can create ANYTHING with almost nothing is an exhilarating feeling. It’s one of the major reasons that keeps me going; the only limit is my imagination.\u003c/p\u003e\u003cp\u003eI absolutely love to travel and experience new cultures (and food!) when the opportunity presents itself. My goal in life is to visit as many places and experience as many different cultures as I possibly can. So far I can cross off Europe and half of Asia from my list, and I look forward to many more exciting adventures to come.\u003c/p\u003e  ",
    gitHub: "AgronKabashi",
    twitter: "_agronkabashi",
    stackOverflow: null,
    linkedIn: "/pub/agron-kabashi/54/6a8/4a6",
    imagePortraitUrl: "https://i.1337co.de/profile/agron-kabashi",
    imageWallOfLeetUrl: "https://i.1337co.de/wallofleet/agron-kabashi",
    highlighted: false,
    published: true,
  },
  {
    name: "Ahmad Mirzaei",
    office: "Stockholm",
    email: "agron.kabashi@tretton37.com",
    phoneNumber: "+46725133736",
    manager: "lotta.wennolf@tretton37.com",
    orgUnit: "/Employees",
    mainText:
      "\u003cp\u003eI have been working in the software industry for over 10 years now, and even though the road has been bumpy, I don’t regret a single second of it! My experiences have been quite varied, from creating games to full blown web-based intranet platforms.\u003c/p\u003e\u003cp\u003eBecause of this variation and these experiences I’ve found that I’ve acquired a thirst for front-end development and anything web related, which can range from mobile apps to web-based SaaS solutions. The fact that you can create ANYTHING with almost nothing is an exhilarating feeling. It’s one of the major reasons that keeps me going; the only limit is my imagination.\u003c/p\u003e\u003cp\u003eI absolutely love to travel and experience new cultures (and food!) when the opportunity presents itself. My goal in life is to visit as many places and experience as many different cultures as I possibly can. So far I can cross off Europe and half of Asia from my list, and I look forward to many more exciting adventures to come.\u003c/p\u003e  ",
    gitHub: "AgronKabashi",
    twitter: "_agronkabashi",
    stackOverflow: null,
    linkedIn: "/pub/agron-kabashi/54/6a8/4a6",
    imagePortraitUrl: "https://i.1337co.de/profile/agron-kabashi",
    imageWallOfLeetUrl: "https://i.1337co.de/wallofleet/agron-kabashi",
    highlighted: false,
    published: true,
  },
];

test("filter people", () => {
  const filteredByName = filterPeople({ name: "Den", office: "" }, list);
  expect(filteredByName.length).toBe(1);

  const filterdByOffice = filterPeople({ name: "", office: "Lu" }, list);
  expect(filterdByOffice.length).toBe(1);

  const filteredByNameAndOffice1 = filterPeople(
    { name: "Den", office: "Lu" },
    list
  );
  expect(filteredByNameAndOffice1.length).toBe(1);

  const filteredByNameAndOffice2 = filterPeople(
    { name: "Den", office: "Sto" },
    list
  );
  expect(filteredByNameAndOffice2.length).toBe(0);
});
