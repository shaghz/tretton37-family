import { NAME, OFFICE } from "../../constants";
import { FilterState } from "../../types";

interface FilterBoxInterface {
  handleChangeValue: React.ChangeEventHandler<HTMLInputElement>;
  searchValue: FilterState;
}

const FilterBox = ({ searchValue, handleChangeValue }: FilterBoxInterface) => {
  return (
    <form className="flex md:flex-row flex-col gap-1">
      <input
        type="text"
        name={NAME}
        value={searchValue.name}
        onChange={handleChangeValue}
        placeholder="filter by name"
        className="w-48 mr-2 mb-2 md:mb-0 bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-trettonGreen-800 "
      />
      <input
        type="text"
        name={OFFICE}
        value={searchValue.office}
        onChange={handleChangeValue}
        placeholder="filter by office"
        className="w-48 bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-trettonGreen-800 "
      />
    </form>
  );
};

export default FilterBox;
