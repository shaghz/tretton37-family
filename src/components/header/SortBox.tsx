interface SortBoxInterface {
  handleChangeValue: React.ChangeEventHandler<HTMLSelectElement>;
}

const SortBox = ({ handleChangeValue }: SortBoxInterface) => {
  return (
    <div className="w-auto sm:flex max-h-11">
      <select
        onChange={handleChangeValue}
        className="w-32 px-5 border-0 sm:mx-2 rounded-sm block w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
      >
        <option>name</option>
        <option>office</option>
      </select>
    </div>
  );
};

export default SortBox;
