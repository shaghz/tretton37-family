interface ViewBoxInterface {
  handleChangeValue: React.MouseEventHandler<HTMLDivElement>;
  isGrid: boolean;
}

const ViewBox = ({ handleChangeValue, isGrid }: ViewBoxInterface) => {
  return (
    <div className="hidden flex-row text-white sm:flex">
      <div
        className={`p-2 border-r-2  w-18 max-h-11 border-2 border-trettonGreen-800 px-4  rounded-tl-sm rounded-bl-sm ${
          isGrid ? "bg-trettonGreen-800 pointer-events-none font-bold" : "cursor-pointer"
        }`}
        onClick={handleChangeValue}
      >
        Grid
      </div>
      <div
        className={`p-2 px-4 border-2 w-18 max-h-11 border-trettonGreen-800 rounded-tr-sm rounded-br-sm  ${
          isGrid ? "cursor-pointer" : "bg-trettonGreen-800 pointer-events-none font-bold"
        }`}
        onClick={handleChangeValue}
      >
        List
      </div>
      <div />
    </div>
  );
};

export default ViewBox;
