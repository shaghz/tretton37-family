const ErrorPage = () => {
  return (
    <div className="App flex items-center flex-col justify-center bg-trettonBlue-800 h-auto">
      <h1 className="text-trettonGreen-800 font-bold uppercase text-6xl mb-2">
        403
      </h1>
      <h1 className="text-trettonGreen-800 font-bold text-4xl mb-6">
        Forbidden
      </h1>
      <div className="text-white">please read ReadMe file</div>
    </div>
  );
};

export default ErrorPage;
