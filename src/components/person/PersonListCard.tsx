import ContactList from "./ContactList";
import AvatarBox from "./AvatarBox";
import { PersonCard } from "./PersonGridCard";

import name from "../../assets/name.svg";
import office from "../../assets/office.svg";

const PersonListCard = ({ person, isGrid }: PersonCard) => {
  return (
    <div
      className={`flex flex-row overflow-hidden shadow-md shadow-Neutral-500/50 bg-zinc-50 p-6 w-full relative`}
      key={person.email}
    >
      <AvatarBox imageUrl={person.imagePortraitUrl} />
      <div className={`flex flex-col mx-6 px-2 py-6 justify-content`}>
        <div className="flex my-2">
          <img src={name} className="w-6 mx-2" alt="name" /> :
          <span className="icon-size mx-2">{person.name}</span>
        </div>
        <div className="flex my-2">
          <img src={office} className="w-6 mx-2" alt="office" /> :
          <span className="icon-size mx-2">{person.office}</span>
        </div>
        <div className="mt-auto">
          <ContactList
            twitter={person.twitter}
            linkedIn={person.linkedIn}
            gitHub={person.gitHub}
            stackOverflow={person.stackOverflow}
            isGrid={isGrid}
          />
        </div>
      </div>
    </div>
  );
};

export default PersonListCard;
