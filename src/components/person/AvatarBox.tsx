import LazyLoad from "react-lazyload";

import avatar from "../../assets/avatar.svg";

const AvatarBox = ({ imageUrl }: { imageUrl: string | null }) => {
  return (
    <div className="flex justify-center rounded-full my-4">
      <LazyLoad height={192} offset={100} once>
        <img
          alt="avatar"
          src={imageUrl || avatar}
          className="w-48 h-48 object-contain rounded-full bg-white"
          loading="lazy"
        />
      </LazyLoad>
    </div>
  );
};

export default AvatarBox;
