import twitterImage from "../../assets/twitter.svg";
import linkedInImage from "../../assets/linkedin.svg";
import stackOverflowImage from "../../assets/stack.svg";
import gitHubImage from "../../assets/github.svg";

interface ContactListInterface {
  twitter: string | null;
  linkedIn: string | null;
  stackOverflow: string | null;
  gitHub: string | null;
  isGrid: boolean;
}

const ContactList = ({
  twitter,
  linkedIn,
  stackOverflow,
  gitHub,
  isGrid,
}: ContactListInterface) => {
  const height = isGrid ? "h-8" : "h-12";
  const width = isGrid ? "w-8" : "w-12";

  return (
    <div className="flex flex-row absolute bottom-10">
      {twitter && (
        <img
          alt="twitter"
          className={`${height} mx-1  cursor-pointer`}
          src={twitterImage}
          onClick={() =>
            window.open(`https://twitter.com/${twitter}`, "_blank")
          }
        />
      )}
      {linkedIn && (
        <img
          alt="linkedin"
          className={`${height} mx-1  cursor-pointer`}
          src={linkedInImage}
          onClick={() =>
            window.open(`https://linkedin.com${linkedIn}`, "_blank")
          }
        />
      )}

      {gitHub && (
        <img
          alt="github"
          src={gitHubImage}
          className={`${height} mx-1  cursor-pointer`}
          onClick={() => window.open(`https://github.com/${gitHub}`, "_blank")}
        />
      )}
      {stackOverflow && (
        <img
          alt="stackoverflow"
          className={`${height} ${width} border-2 p-1 border-orange-300 rounded-full mx-1  cursor-pointer`}
          src={stackOverflowImage}
          onClick={() =>
            window.open(
              `https://stackOverflow.com/users/${stackOverflow}`,
              "_blank"
            )
          }
        />
      )}
    </div>
  );
};

export default ContactList;
