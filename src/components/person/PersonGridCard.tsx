import ContactList from "./ContactList";
import AvatarBox from "./AvatarBox";

import { Person } from "../../types";

import name from "../../assets/name.svg";
import office from "../../assets/office.svg";

export interface PersonCard {
  person: Person;
  isGrid: boolean;
}

const PersonGridCard = ({ person, isGrid }: PersonCard) => {
  return (
    <div
      className={`overflow-hidden shadow-md shadow-Neutral-500/50 bg-zinc-50 p-2 px-4 w-full relative min-w-[250px]`}
      key={person.email}
    >
      <AvatarBox imageUrl={person.imagePortraitUrl} />
      <div className="flex my-2">
        <img src={name} className="icon-size w-6 mx-2" alt="name" /> :
        <span className="mx-2">{person.name}</span>
      </div>
      <div className="flex my-2">
        <img src={office} className=" icon-sizew-6 mx-2" alt="office" /> :
        <span className="mx-2">{person.office}</span>
      </div>
      <div className="mt-auto mb-24">
        <ContactList
          twitter={person.twitter}
          linkedIn={person.linkedIn}
          gitHub={person.gitHub}
          stackOverflow={person.stackOverflow}
          isGrid={isGrid}
        />
      </div>
    </div>
  );
};

export default PersonGridCard;
