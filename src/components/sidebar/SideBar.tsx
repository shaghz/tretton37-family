import menu from "../../assets/menu.svg";

interface SideBarInterface {
  showSidebar: boolean;
  handleChangeValue: React.MouseEventHandler<HTMLImageElement>;
  renderTools: () => React.ReactNode;
}

const SideBar = ({
  showSidebar,
  handleChangeValue,
  renderTools,
}: SideBarInterface) => {
  if (!showSidebar) {
    return null;
  }
  return (
    <aside className="w-full bg-trettonBlue-800 min-h-screen px-3">
      <div className="flex bg-trettonBlue-800 sm:p-5 w-full py-4 px-1 justify-between items-center sm:flex-row-reverse">
        <h2 className="text-trettonGreen-800 mr-1 sm:mr-auto font-bold text-xl">
          Tretton37
        </h2>
        <img
          src={menu}
          alt="menu"
          className="flex sm:hidden w-8"
          onClick={handleChangeValue}
        />
      </div>
      <div className="flex flex-col-reverse justify-space gap-y-4 mt-8">
        {renderTools()}
      </div>
    </aside>
  );
};

export default SideBar;
