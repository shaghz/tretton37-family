import { useEffect, useReducer, useRef, useState } from "react";

// components
import FilterBox from "./components/header/FilterBox";
import SortBox from "./components/header/SortBox";
import ViewBox from "./components/header/ViewBox";
import SideBar from "./components/sidebar/SideBar";
import ErrorPage from "./components/error/ErrorPage";
import LoadingPage from "./components/loading/LoadingPage";
import PersonGridCard from "./components/person/PersonGridCard";
import PersonListCard from "./components/person/PersonListCard";

// other data
import { NAME, OFFICE } from "./constants";
import { FilterState, Person } from "./types";

//assets and css
import menu from "./assets/menu.svg";
import "./App.css";
import { sortByValue } from "./util/sortByValue";
import { filterPeople } from "./util/filterPeople";

const URL = "https://api.1337co.de/v3/employees";

function App() {
  const [people, setPeople] = useState<Person[]>([]);
  const [tempPeople, setTempPeople] = useState<Person[]>([]);
  const [loading, setLoading] = useState(true);
  const [isGrid, setIsGrid] = useState(true);
  const [error, setError] = useState(false);
  const [showSidebar, setShowSidebar] = useState(false);

  const timeoutId: any = useRef();

  const [filterInput, setFilterInput] = useReducer(
    (state: FilterState, newState: FilterState) => ({ ...state, ...newState }),
    {
      name: "",
      office: "",
    }
  );

  // FETCH DATA FROM SERVER
  const fetchData = async () => {
    if (!process.env.REACT_APP_API_KEY) {
      setError(true);
      return;
    }
    setLoading(true);
    try {
      await fetch(URL, {
        headers: {
          Authorization: process.env.REACT_APP_API_KEY || "",
        },
      }).then(async (response) => {
        const data = await response.json();
        if (response.ok) {
          setPeople(data);
          setTempPeople(data);
          setLoading(false);
        }
      });
    } catch (error) {
      console.error(error);
      setLoading(false);
    }
  };

  useEffect(() => {
    fetchData();
    return () => {
      clearTimeout(timeoutId.current);
    };
  }, []);

  // SORT
  const handleSortPeople = (event: React.ChangeEvent<HTMLSelectElement>) => {
    const allPeople = [...people];
    const value = event.target.value === OFFICE ? OFFICE : NAME;
    setPeople(sortByValue(value, allPeople));
  };

  // FILTER
  const handleFilterPeople = (event: React.ChangeEvent<HTMLInputElement>) => {
    const { name, value } = event.target;
    setFilterInput({ [name]: value });
  };

  useEffect(() => {
    const allPeople = [...tempPeople];
    setPeople(filterPeople(filterInput, allPeople));
  }, [filterInput, tempPeople]);

  // LOADING ON GRID

  const handleChangeView = () => {
    setLoading(true);
    setIsGrid((currentView) => !currentView);
    timeoutId.current = window.setTimeout(() => {
      setLoading(false);
    }, 2000);
  };

  // OTHER RENDERS

  const renderTools = () => (
    <>
      <FilterBox
        searchValue={filterInput}
        handleChangeValue={handleFilterPeople}
      />
      <SortBox handleChangeValue={handleSortPeople} />
      <ViewBox isGrid={isGrid} handleChangeValue={handleChangeView} />
    </>
  );

  const renderNoResult = () => (
    <div className="mt-8 p-16 font-bold">Sorry! No result was found :(</div>
  );

  if (error) {
    return <ErrorPage />;
  }

  return (
    <div className="App flex items-center flex-col">
      <SideBar
        showSidebar={showSidebar}
        renderTools={renderTools}
        handleChangeValue={() =>
          setShowSidebar((currentShow: boolean) => !currentShow)
        }
      />
      <div className="flex bg-trettonBlue-800 sm:p-5 w-full shadow-md p-2 justify-between items-center sm:flex-row-reverse">
        <div className="hidden sm:flex sm:space-between">{renderTools()}</div>
        <div className="flex w-full justify-between p-2">
          <h2 className="text-trettonGreen-800 mr-1 sm:mr-auto font-bold text-xl">
            Tretton37
          </h2>
          <img
            alt="menu"
            src={menu}
            className="flex sm:hidden w-8 cursor-pointer"
            onClick={() =>
              setShowSidebar((currentShow: boolean) => !currentShow)
            }
          />
        </div>
      </div>
      {loading ? (
        <LoadingPage />
      ) : people.length === 0 ? (
        renderNoResult()
      ) : (
        <div
          className={
            isGrid
              ? `grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-4 gap-4 md:gap-6 p-5 pt-6 sm:px-24`
              : `grid gap-4 md:gap-6 p-5 w-full pt-6 sm:px-24`
          }
        >
          {people.map((person: Person) =>
            isGrid ? (
              <PersonGridCard
                person={person}
                isGrid={isGrid}
                key={person.email}
              />
            ) : (
              <PersonListCard
                person={person}
                isGrid={isGrid}
                key={person.email}
              />
            )
          )}
        </div>
      )}
    </div>
  );
}

export default App;
